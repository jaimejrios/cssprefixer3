# CSSPrefixer3

A Python3 tool to rewrite and minify your CSS files. It adds vendor-prefixed versions of CSS3 rules (similar functionality to [AutoPrefixer](https://github.com/postcss/autoprefixer)).

CSSPrefixer3 supports many CSS3 properties including keyframe animations, flexbox and gradients.

For example, this
```css
#wrapper {
    border-radius: 1em;
    transform: rotate(45deg)
}
```

becomes this:
```css
#wrapper {
    -moz-border-radius: 1em;
    -webkit-border-radius: 1em;
    border-radius: 1em;
    -o-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg)
}
```

This is an update to [CSSPrefixer2](https://pypi.python.org/pypi/cssprefixer2) to enable Python3 compatibility.

Included binary file is based off the original [CSSPrefixer](https://pypi.python.org/pypi/cssprefixer).

## How to Install

1) Install pip3 using these [instructions](https://pip.pypa.io/en/stable/installation/).
2) Clone the CSSPrefixer3 repo into `~/Downloads`, then `cd` into it.
3) Copy the `bin/cssprefixer3` file into `~/.local/bin/`
4) Copy the rest of the files and folders in the repo into `~/.local/bin/python3.8/site-packages/`

## How to use CSSPrefixer3

From your terminal, enter the following to minify and prefix your css...\
`cssprefixer3 styles-1.css styles-2.css --minify > styles.css`

The --minify flag is optional.
